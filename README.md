TP Final PHP Framework

Vous devez mettre en œuvre une gestion de promotion, d'élèves, et de modules.
Les Tables & relations
Les tables devront contenir au minimum les informations suivantes :

Promo
- nom : B1, B2, etc
- spécialité : info, design, 3d, etc...

Module
- nom
- description

Eleve
- nom
- prénom
- email

Les relations :

- 1 promo : n élèves
- 1 promotion : n modules

- 1 élève : 1 promo
- 1 élève : n modules

- 1 module : n élèves
- 1 module : n promotions
