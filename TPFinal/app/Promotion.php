<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['name', 'speciality'];

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function modules() {
        return $this->belongsToMany(Module::class);
    }
}
