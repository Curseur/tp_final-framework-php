<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\Student;
use App\Module;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        $search = $request->get("search");
        if ($search) {
            $promotion = Promotion::where('name', 'like', '%' . $search . '%')
            ->orWhere('speciality', 'like', '%' . $search . '%')
            ->get();
        // } elseif ($search) {
        //     $student = Student::where('lastname', 'like', '%' . $search . '%')
        //     ->orWhere('firstname', 'like', '%' . $search . '%')
        //     ->get();
        // }elseif ($search) {
        //     $module = Module::where('name', 'like', '%' . $search . '%')
        //     ->get();
        } else {
          $promotion = Promotion::all();
        //   $student = Student::all();
        //   $module = Module::all();
        }

        // return view("promotion.index", ["promotions" => $promotion, "students" => $student, "modules" => $module, "search" => $search]);
        return view("promotion.index", ["promotions" => $promotion, "search" => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("promotion.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promotion = new Promotion();
        $promotion->name = $request->input("name");
        $promotion->speciality = $request->input("speciality");
        $promotion->save();

        return redirect()->action([PromotionController::class, 'index']);
    }

    public function storeModules(Request $request)
    {
        $promotion = Promotion::find($request->input("promotion_id"));
        $promotion->modules()->detach();
        $promotion->modules()->attach($request->input("modules"));

        return redirect()->route("module.index", ["promotion" => $promotion->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        return view("promotion.show", ["promo" => $promotion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        return view("promotion.edit", ["promo" => $promotion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $promo = Promotion::find($promotion->id);
        $promo->update([
            "name" => $request->input("name"),
            "speciality" => $request->input("speciality"),
        ]);

        return redirect()->action([PromotionController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        $promotion->students()->delete();
        // $promotion->modules()->delete();
        $promotion->delete();
        return redirect()->action([PromotionController::class, 'index']);
    }
}
