<?php

namespace App\Http\Controllers;

use App\Module;
use App\Promotion;
use App\Student;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): View
    {
        $search = $request->get("search");
        if ($search) {
            $module = Module::where('name', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->get();
        // } elseif ($search) {
        //     $promotion = Promotion::where('name', 'like', '%' . $search . '%')
        //     ->orWhere('speciality', 'like', '%' . $search . '%')
        //     ->get();
        // }elseif ($search) {
        //     $student = Student::where('lastname', 'like', '%' . $search . '%')
        //     ->orWhere('firstname', 'like', '%' . $search . '%')
        //     ->get();
        } else {
            $module = Module::all();
            // $promotion = Promotion::all();
            // $student = Student::all();
        }

        // return view("module.index", ["modules" => $module, "promotions" => $promotion, "student" => $student, "search" => $search, "current_promotion_id" => $request->get("promotion"), "current_student_id" => $request->get("student"), "modules" => Module::all()]);
        return view("module.index", ["modules" => $module, "search" => $search, "current_promotion_id" => $request->get("promotion"), "current_student_id" => $request->get("student")]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request): View
    {
        return view(
            "module.create",
            [
                "promotions" => Promotion::all(),
                "students" => Student::all(),
                "current_promotion_id" => $request->input("promotion_id"),
                "current_student_id" => $request->input("student_id")
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $newModule = new Module();
        $newModule->name = $request->input("name");
        $newModule->description = $request->input("description");
        $newModule->save();

        $newModule->promotions()->attach($request->input("promotions"));
        $newModule->students()->attach($request->input("students"));

        return redirect()->route(
            "module.index",
            [
                "promotion" => $request->get("promotion_id"),
                "student" => $request->get("student_id")
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module): View
    {
        return view("module.show", ["current_module" => $module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module): View
    {
        return view("module.edit",
            [
                "promotions" => Promotion::all(),
                "students" => Student::all(),
                "editing_module" => $module,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module): RedirectResponse
    {
        $module->name = $request->input("name");
        $module->description = $request->input("description");
        $module->promotions()->detach();
        $module->students()->detach();
        $module->promotions()->attach($request->input("promotions"));
        $module->students()->attach($request->input("students"));
        $module->save();

        return redirect()->route("module.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module): RedirectResponse
    {
        $module->delete();
        return redirect()->route("module.index");
    }
}
