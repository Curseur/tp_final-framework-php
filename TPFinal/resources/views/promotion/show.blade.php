@extends('application')
@section('page-title')
  {{ $promo->name }}
@endsection

@section('page-content')
  <div class="container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">{{ $promo->name }} - {{ $promo->speciality }}</h5>
        <p class="card-text">
          <strong>Students promotion: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
            @foreach($promo->students as $stud)
                <li class="list-group-item">
                    <a href="{{ route("student.show", $stud) }}">{{ $stud->lastname." ".$stud->firstname }}</a>
                </li>
            @endforeach
        </ul>
        <p class="card-text">
          <strong>Modules promotion: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
            @foreach($promo->modules as $modu)
                <li class="list-group-item">
                    <a href="{{ route("module.show", $modu) }}">{{ $modu->name." ".$modu->speciality }}</a>
                </li>
            @endforeach
        </ul>
        <div class="d-flex">
            <a class="btn btn-outline-primary mr-2" href="{{ route("student.create", ["promotion"=>$promo]) }}">Add Student</a>
            <a class="btn btn-outline-primary mr-2" href="{{ route("module.index", ["promotion"=>$promo]) }}">Add Modules</a>
            <a class="btn btn-outline-info mr-2" href="{{ route("promotion.edit", $promo) }}">Edit</a>
            <form action="{{ route("promotion.destroy", $promo->id) }}" method="post">
                <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                @method('delete')
                @csrf
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
