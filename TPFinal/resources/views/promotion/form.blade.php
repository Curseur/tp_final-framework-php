@extends('application')
@section('page-title')
  @yield('page-title')
@endsection
@section('page-content')
  <div class="container mb-5 mt-3">
    <form method="post" action="@yield('action')">
      @yield('method')
      @csrf
        <div class="form-group">
          <label for="name">Promo Name</label>
          {{-- <input type="text" class="form-control" name="name" id="name" value="@yield("name")"> --}}
          <select class="form-control" name="name" id="name">
            <option>B1</option>
            <option>B2</option>
            <option>B3</option>
            <option>M1</option>
            <option>M2</option>
          </select>
        </div>
        <div class="form-group">
          <label for="speciality">Speciality</label>
          {{-- <input type="text" class="form-control" name="speciality" id="speciality" value="@yield("speciality")"> --}}
          <select class="form-control" name="speciality" id="speciality">
            <option>Computer Science</option>
            <option>Robotics & Systems Engineering</option>
            <option>Marketing Communication</option>
            <option>Audiovisual</option>
            <option>3D Animation Video games</option>
            <option>Creation Design</option>
          </select>
        </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection
