@extends("module.form")
@section("title")Create Module @endsection
@section("action")
	{{ route("module.store", ["promotion_id" => $current_promotion_id, "student_id" => $current_student_id]) }}
@endsection
@section("promotions-list")
	@foreach($promotions as $promotion)
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="promotion-{{ $promotion->id }}"
						 value="{{ $promotion->id }}" name="promotions[]"
						 	@if($current_promotion_id != null && $promotion->id == $current_promotion_id) checked @endif>
			<label class="form-check-label" for="promotion-{{ $promotion->id }}">{{ $promotion->name." ".$promotion->speciality }}</label>
		</div>
	@endforeach
@endsection
@section("students-list")
	@foreach($students as $student)
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="student-{{ $student->id }}"
						 value="{{ $student->id }}" name="students[]"
						 	@if($current_student_id != null && $student->id == $current_student_id) checked @endif>
			<label class="form-check-label" for="student-{{ $student->id }}">{{ $student->lastname." ".$student->firstname }}</label>
		</div>
	@endforeach
@endsection