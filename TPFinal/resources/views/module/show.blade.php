@extends("application")

@section("page-title")
	{{ $current_module->name }}
@endsection
@section("page-content")
	<div class="container d-flex justify-content-center">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">{{ "Module : ".$current_module->name }}</h5>
				<h6 class="card-subtitle mb-2 text-muted">{{ "Description : ".$current_module->description }}</h6>
				<p class="mt-5">Promotions accessing the module:</p>
				<ul class="list-group mb-5">
					@foreach($current_module->promotions as $module_promotion)
						<li class="list-group-item" aria-current="true">{{ $module_promotion->name }}</li>
					@endforeach
				</ul>
				<p class="mt-5">Students accessing the module:</p>
				<ul class="list-group mb-5">
					@foreach($current_module->students as $module_student)
						<li class="list-group-item" aria-current="true">{{ $module_student->lastname." ".$module_student->firstname }}</li>
					@endforeach
				</ul>

				<div class="d-flex">
					<a href="{{ route("module.edit", $current_module) }}"
						 class="btn btn-outline-info card-link mr-3">Edit</a>
					<form action="{{ route("module.destroy", $current_module) }}" method="post">
						<input class="btn btn-outline-danger" type="submit" value="Delete"/>
						@method('delete')
						@csrf
					</form>
				</div>

			</div>
		</div>
	</div>
@endsection
