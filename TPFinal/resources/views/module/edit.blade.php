@extends("module.form")
@section("title")Edit Module @endsection
@section("action"){{ route("module.update", $editing_module) }} @endsection
@section("method") @method("PUT") @endsection
@section("name"){{ $editing_module->name }}@endsection
@section("description"){{ $editing_module->description }} @endsection
@section("promotions-list")
	@foreach($promotions as $promotion)
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="promotion-{{ $promotion->id }}"
						 value="{{ $promotion->id }}" name="promotions[]"
						 	@foreach($editing_module->promotions as $module_promotion)
						 		@if($module_promotion->id == $promotion->id) checked @endif
						 	@endforeach
			>
			<label class="form-check-label" for="promotion-{{ $promotion->id }}">{{ $promotion->name." ".$promotion->speciality }}</label>
		</div>
	@endforeach
@endsection
@section("students-list")
	@foreach($students as $student)
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="student-{{ $student->id }}"
						 value="{{ $student->id }}" name="students[]"
						 	@foreach($editing_module->students as $module_student)
						 		@if($module_student->id == $student->id) checked @endif
						 	@endforeach
			>
			<label class="form-check-label" for="student-{{ $student->id }}">{{ $student->lastname." ".$student->firstname }}</label>
		</div>
	@endforeach
@endsection