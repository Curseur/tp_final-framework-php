

<?php $__env->startSection('page-title'); ?>
    Edit Promotion
<?php $__env->stopSection(); ?>

<?php $__env->startSection('action'); ?><?php echo e(route('promotion.update', $promo)); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection("method"); ?><?php echo method_field("PUT"); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("name"); ?><?php echo e($promo->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("speciality"); ?><?php echo e($promo->speciality); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("promotion.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/promotion/edit.blade.php ENDPATH**/ ?>