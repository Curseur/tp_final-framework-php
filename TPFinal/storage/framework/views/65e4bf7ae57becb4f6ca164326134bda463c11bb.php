
<?php $__env->startSection('page-title'); ?>
    <?php echo e($student->lastname); ?> <?php echo e($student->firstname); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-content'); ?>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?php echo e($student->lastname); ?> <?php echo e($student->firstname); ?></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?php echo e($student->promotion->name." ".$student->promotion->speciality); ?></h6>
                <p class="card-text">
                    <strong>Email: </strong>
                    <?php echo e($student->email); ?>

                </p>
                <p class="card-text">
                    <strong>Modules: </strong>
                </p>
                <ul class="list-group list-group-flush mb-3">
                    <?php $__currentLoopData = $student->modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="list-group-item">
                            <a href="<?php echo e(route("module.show", $modu)); ?>"><?php echo e($modu->name); ?></a>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <div class="d-flex">
                    <a class="btn btn-outline-primary mr-2" href="<?php echo e(route("module.index", ["student"=>$student])); ?>">Add Modules</a>
                    <a class="btn btn-outline-info mr-2" href="<?php echo e(route("student.edit", $student)); ?>">Edit</a>
                    <form action="<?php echo e(route("student.destroy", $student->id)); ?>" method="post">
                        <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                        <?php echo method_field('delete'); ?>
                        <?php echo csrf_field(); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/student/show.blade.php ENDPATH**/ ?>