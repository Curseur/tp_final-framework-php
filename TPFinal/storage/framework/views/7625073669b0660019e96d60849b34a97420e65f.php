
<?php $__env->startSection('page-title'); ?>
	<?php echo $__env->yieldContent("title"); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
	<?php if(!isset($current_promotion_id)): ?> <?php echo e($current_promotion_id = null); ?>  <?php endif; ?>
	<div class="container">
		<form method="POST" action="<?php echo $__env->yieldContent("action"); ?>">
			<?php echo csrf_field(); ?>
			<?php echo $__env->yieldContent("method"); ?>
			<div class="mb-3">
				<label for="name" class="form-label">Module Name</label>
				<input type="text" class="form-control" id="name" name="name" value="<?php echo $__env->yieldContent("name"); ?>">
			</div>
			<div class="mb-3">
				<label for="description" class="form-label">Description</label>
				<input type="text" class="form-control" id="description" name="description" value="<?php echo $__env->yieldContent("description"); ?>">
			</div>
			<div class="mb-3">
			<label for="name" class="form-label">Promotions List</label>
			<?php echo $__env->yieldContent("promotions-list"); ?>
			</div>
			<div class="mb-3">
			<label for="name" class="form-label">Students List</label>
			<?php echo $__env->yieldContent("students-list"); ?>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/form.blade.php ENDPATH**/ ?>