
<?php $__env->startSection('page-title'); ?>
  <?php echo e($promo->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-content'); ?>
  <div class="container">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title"><?php echo e($promo->name); ?> - <?php echo e($promo->speciality); ?></h5>
        <p class="card-text">
          <strong>Students promotion: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
            <?php $__currentLoopData = $promo->students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stud): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="list-group-item">
                    <a href="<?php echo e(route("student.show", $stud)); ?>"><?php echo e($stud->lastname." ".$stud->firstname); ?></a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        <p class="card-text">
          <strong>Modules promotion: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
            <?php $__currentLoopData = $promo->modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="list-group-item">
                    <a href="<?php echo e(route("module.show", $modu)); ?>"><?php echo e($modu->name." ".$modu->speciality); ?></a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        <div class="d-flex">
            <a class="btn btn-outline-primary mr-2" href="<?php echo e(route("student.create", ["promotion"=>$promo])); ?>">Add Student</a>
            <a class="btn btn-outline-primary mr-2" href="<?php echo e(route("module.index", ["promotion"=>$promo])); ?>">Add Modules</a>
            <a class="btn btn-outline-info mr-2" href="<?php echo e(route("promotion.edit", $promo)); ?>">Edit</a>
            <form action="<?php echo e(route("promotion.destroy", $promo->id)); ?>" method="post">
                <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                <?php echo method_field('delete'); ?>
                <?php echo csrf_field(); ?>
            </form>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/promotion/show.blade.php ENDPATH**/ ?>