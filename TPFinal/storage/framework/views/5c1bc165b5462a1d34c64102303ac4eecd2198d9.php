
<?php $__env->startSection('page-title'); ?>
    List
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
    <div class="container">

        <?php if($search): ?>
            <p class="mt-3">Search result for: <?php echo e($search); ?></p>
            <a href="<?php echo e(route("student.index")); ?>" class="mb-5">Return to list</a>
        <?php endif; ?>

        <table class="table table-bordered mt-3">
            <thead>
            <tr>
                <th scope="col">Lastname</th>
                <th scope="col">Firstname</th>
                <th scope="col">Email</th>
                <th scope="col">Promotion</th>
                <th scope="col">Modules List</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            <?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stud): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($stud->lastname); ?></td>
                    <td><?php echo e($stud->firstname); ?></td>
                    <td><?php echo e($stud->email); ?></td>
                    <td><?php echo e($stud->promotion->name." ".$stud->promotion->speciality); ?></td>
                    <td>
                        <?php $__currentLoopData = $stud->modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					        <li class="list-group" aria-current="true"><?php echo e($modu->name); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </td>
                    
                    <td class="d-flex" style="size: inherit">
                        <a class="btn btn-outline-success mr-2" href="<?php echo e(route("student.show", $stud)); ?>">Show</a>
                        <a class="btn btn-outline-info mr-2" href="<?php echo e(route("student.edit", $stud)); ?>">Edit</a>
                        <form action="<?php echo e(route("student.destroy", $stud->id)); ?>" method="post">
                            <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                            <?php echo method_field('delete'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/student/index.blade.php ENDPATH**/ ?>