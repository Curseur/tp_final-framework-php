
<?php $__env->startSection("title"); ?>Create Module <?php $__env->stopSection(); ?>
<?php $__env->startSection("action"); ?>
	<?php echo e(route("module.store", ["promotion_id" => $current_promotion_id, "student_id" => $current_student_id])); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("promotions-list"); ?>
	<?php $__currentLoopData = $promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="promotion-<?php echo e($promotion->id); ?>"
						 value="<?php echo e($promotion->id); ?>" name="promotions[]"
						 	<?php if($current_promotion_id != null && $promotion->id == $current_promotion_id): ?> checked <?php endif; ?>>
			<label class="form-check-label" for="promotion-<?php echo e($promotion->id); ?>"><?php echo e($promotion->name." ".$promotion->speciality); ?></label>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection("students-list"); ?>
	<?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="student-<?php echo e($student->id); ?>"
						 value="<?php echo e($student->id); ?>" name="students[]"
						 	<?php if($current_student_id != null && $student->id == $current_student_id): ?> checked <?php endif; ?>>
			<label class="form-check-label" for="student-<?php echo e($student->id); ?>"><?php echo e($student->lastname." ".$student->firstname); ?></label>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("module.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/create.blade.php ENDPATH**/ ?>