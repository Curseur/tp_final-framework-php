
<?php $__env->startSection('page-title'); ?>Modules <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
  <div class="container">
    <?php if($search): ?>
      <p class="mt-3">Search result for: <?php echo e($search); ?></p>
      <a href="<?php echo e(route("module.index")); ?>" class="mb-5">Return to list</a>
    <?php endif; ?>
  </div>
  <div class="container">
    <?php if(isset($current_module_id)): ?>
      <?php echo $__env->make("module.parts.form_create", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?>
      <?php echo $__env->make("module.parts.list", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/index.blade.php ENDPATH**/ ?>