
<?php $__env->startSection("title"); ?>Edit Module <?php $__env->stopSection(); ?>
<?php $__env->startSection("action"); ?><?php echo e(route("module.update", $editing_module)); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection("method"); ?> <?php echo method_field("PUT"); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection("name"); ?><?php echo e($editing_module->name); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection("description"); ?><?php echo e($editing_module->description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection("promotions-list"); ?>
	<?php $__currentLoopData = $promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="promotion-<?php echo e($promotion->id); ?>"
						 value="<?php echo e($promotion->id); ?>" name="promotions[]"
						 	<?php $__currentLoopData = $editing_module->promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module_promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						 		<?php if($module_promotion->id == $promotion->id): ?> checked <?php endif; ?>
						 	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			>
			<label class="form-check-label" for="promotion-<?php echo e($promotion->id); ?>"><?php echo e($promotion->name." ".$promotion->speciality); ?></label>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection("students-list"); ?>
	<?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="student-<?php echo e($student->id); ?>"
						 value="<?php echo e($student->id); ?>" name="students[]"
						 	<?php $__currentLoopData = $editing_module->students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module_student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						 		<?php if($module_student->id == $student->id): ?> checked <?php endif; ?>
						 	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			>
			<label class="form-check-label" for="student-<?php echo e($student->id); ?>"><?php echo e($student->lastname." ".$student->firstname); ?></label>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("module.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/edit.blade.php ENDPATH**/ ?>