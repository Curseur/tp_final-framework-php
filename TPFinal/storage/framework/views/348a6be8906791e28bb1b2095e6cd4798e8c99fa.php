
<?php $__env->startSection("page-title"); ?>Add Student <?php $__env->stopSection(); ?>
<?php $__env->startSection("action"); ?>
	<?php echo e(route("student.store")); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('promotion'); ?><?php echo e($promotion->name." ".$promotion->speciality); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('promotion_id'); ?><?php echo e($promotion->id); ?><?php $__env->stopSection(); ?>


<?php echo $__env->make("student.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/student/create.blade.php ENDPATH**/ ?>