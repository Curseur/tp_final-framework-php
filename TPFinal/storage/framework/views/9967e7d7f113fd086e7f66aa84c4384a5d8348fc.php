

<?php $__env->startSection("page-title"); ?>
	<?php echo e($current_module->name); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("page-content"); ?>
	<div class="container d-flex justify-content-center">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title"><?php echo e("Module : ".$current_module->name); ?></h5>
				<h6 class="card-subtitle mb-2 text-muted"><?php echo e("Description : ".$current_module->description); ?></h6>
				<p class="mt-5">Promotions accessing the module:</p>
				<ul class="list-group mb-5">
					<?php $__currentLoopData = $current_module->promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module_promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="list-group-item" aria-current="true"><?php echo e($module_promotion->name); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
				<p class="mt-5">Students accessing the module:</p>
				<ul class="list-group mb-5">
					<?php $__currentLoopData = $current_module->students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module_student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="list-group-item" aria-current="true"><?php echo e($module_student->lastname." ".$module_student->firstname); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>

				<div class="d-flex">
					<a href="<?php echo e(route("module.edit", $current_module)); ?>"
						 class="btn btn-outline-info card-link mr-3">Edit</a>
					<form action="<?php echo e(route("module.destroy", $current_module)); ?>" method="post">
						<input class="btn btn-outline-danger" type="submit" value="Delete"/>
						<?php echo method_field('delete'); ?>
						<?php echo csrf_field(); ?>
					</form>
				</div>

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("application", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/show.blade.php ENDPATH**/ ?>