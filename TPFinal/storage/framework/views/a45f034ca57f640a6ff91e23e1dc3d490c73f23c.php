
<?php $__env->startSection('page-title'); ?>
  <?php echo $__env->yieldContent('page-title'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
  <div class="container mb-5 mt-3">
    <form method="post" action="<?php echo $__env->yieldContent('action'); ?>">
      <?php echo $__env->yieldContent('method'); ?>
      <?php echo csrf_field(); ?>
        <div class="form-group">
          <label for="name">Promo Name</label>
          
          <select class="form-control" name="name" id="name">
            <option>B1</option>
            <option>B2</option>
            <option>B3</option>
            <option>M1</option>
            <option>M2</option>
          </select>
        </div>
        <div class="form-group">
          <label for="speciality">Speciality</label>
          
          <select class="form-control" name="speciality" id="speciality">
            <option>Computer Science</option>
            <option>Robotics & Systems Engineering</option>
            <option>Marketing Communication</option>
            <option>Audiovisual</option>
            <option>3D Animation Video games</option>
            <option>Creation Design</option>
          </select>
        </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/promotion/form.blade.php ENDPATH**/ ?>