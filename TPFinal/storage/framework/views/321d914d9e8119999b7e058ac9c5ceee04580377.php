<a class="btn btn-outline-success mt-3" href="<?php echo e(route("module.create")); ?>">Create Module</a>

<table class="table table-bordered mt-3">
	<thead>
	<tr>
		<th scope="col">Module Name</th>
		<th scope="col">Description</th>
		<th scope="col">Promotions List</th>
		<th scope="col">Students List</th>
		<th scope="col">Actions</th>
	</tr>
	</thead>
	<tbody>

	<?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e($modu->name); ?></td>
			<td><?php echo e($modu->description); ?></td>
			<td>
				<?php $__currentLoopData = $modu->promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li class="list-group" aria-current="true"><?php echo e($promo->name." ".$promo->speciality); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</td>
			<td>
				<?php $__currentLoopData = $modu->students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stud): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li class="list-group" aria-current="true"><?php echo e($stud->lastname." ".$stud->firstname); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</td>

			<td class="d-flex" style="size: inherit">
				<a class="btn btn-outline-success mr-2" href="<?php echo e(route("module.show", $modu)); ?>">Show</a>
				<a class="btn btn-outline-info mr-2" href="<?php echo e(route("module.edit", $modu)); ?>">Edit</a>
				<form action="<?php echo e(route("module.destroy", $modu->id)); ?>" method="post">
					<input class="btn btn-outline-danger" type="submit" value="Delete"/>
					<?php echo method_field('delete'); ?>
					<?php echo csrf_field(); ?>
				</form>
			</td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	</tbody>
</table>
<?php /**PATH C:\laragon\www\PHPFinal\resources\views/module/parts/list.blade.php ENDPATH**/ ?>