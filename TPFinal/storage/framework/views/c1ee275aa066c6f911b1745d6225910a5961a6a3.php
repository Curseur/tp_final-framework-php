
<?php $__env->startSection("page-title"); ?>
    Add Student
<?php $__env->stopSection(); ?>
<?php $__env->startSection("action"); ?><?php echo e(route("student.update", $student)); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection("method"); ?> <?php echo method_field("PUT"); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection("lastname"); ?><?php echo e($student->lastname); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection("firstname"); ?><?php echo e($student->firstname); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection("email"); ?><?php echo e($student->email); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection("promotion"); ?>
    <select name="promotion" class="form-control">
        <?php $__currentLoopData = $promotion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($promo->id); ?>"><?php echo e($promo->name." ".$promo->speciality); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("student.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/student/edit.blade.php ENDPATH**/ ?>