
<?php $__env->startSection('page-title'); ?>
    <?php echo $__env->yieldContent('page-title'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
    <div class="container mb-5 mt-3">
        <form method="post" action="<?php echo $__env->yieldContent('action'); ?>">
            <?php echo csrf_field(); ?>
            <?php echo method_field("put"); ?>
            <div class="form-row">
                <div class="col">
                    <label for="lastname">Lastname</label>
                    <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo $__env->yieldContent("lastname"); ?>">
                </div>
                <div class="col">
                    <label for="firstname">Firstname</label>
                    <input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo $__env->yieldContent("firstname"); ?>">
                </div>
                <div class="col">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" value="<?php echo $__env->yieldContent("email"); ?>">
                </div>
            </div>
            <label for="promotion">Promotion</label>
            <fieldset>
                <div class="form-row mb-5">
                  <div class="col">
                    <?php echo $__env->yieldContent("promotion"); ?>
                  </div>
                </div>
            </fieldset>
            <input type="hidden" name="promotion_id" value="<?php echo $__env->yieldContent("promotion_id"); ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('application', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\PHPFinal\resources\views/student/form.blade.php ENDPATH**/ ?>